# CONTRIBUTING
Please note the contents of this document are still being completed.
> Always remember we are working with (.uassets) binary files.  
> Be mindful of what blueprint classes are involved with the [issue](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/issues).  
> Please assign [labels](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/labels) to help others identify if there may be a conflict.

# How to contribute
> I would like to first thank you for contributing your time to get this far, If you have not already,  
> reach out to us on [Discord](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/Discord) to discuss this if it's your first time tackling an [issue](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/issues).  

##### Typical workflow  
1. Check the [Issue Board](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/boards) for something you can work on
1. Assign yourself the issue or choose one assigned to you already.
1. Create a merge request (from in the issue.)
1. Check out the branch for the issue 
1. Do the work
1. Pull down any commits to workout any conflicts
1. Push up your changes
1. Complete the merge request.
1. Throw yourself a party, because **YOU'RE AWSEOME!**

# Create Merge Request from Issue
This step is crucial as you will need to ensure your merge back into our development copy is free of problems.  
>  Be sure you are actively working in the correct branch!

- Access your [issue](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/issues) card.
- Select `Create merge request`
- Copy to clipboard the branch name ie: `73-create-shot-animation`
- Ensure source reads ``develop``
- Checkout repository
  - Locate your Repository folder
  - Right click and select `Git Bash Here`
  - Run `git checkout -b 73-create-shot-animation`  
Return:
`Switched to a new branch '73-create-shot-animation'`

# Add new files
In the process of completeing my issue i have generated new assets that will need to be added to the Gitlab repository.
> Additional steps may be required if you are adding textures, music, or models.  
> Generally however new Blueprints or Source files are expected.

- Add new files to the repository.
  - Locate your Repository folder
  - Right click and select `Git Bash Here`
  - Run `git add .`  
Should add all files based on the rules in the [.gitignore](.gitignore).

# Commit changes
Your changes have been completed its time to commit to them.
> This will provide updates to the issue feeding out to respected resources.  
> Not just the `what` is important but the `how` and `why` when possible. 
- Save all and close out of unreal engine.
- Next you will need to commit your changes.
  - Locate your Repository folder
  - Right click and select `Git Bash Here`
  - Run `git commit -m "#73 Meaningful comment about the work I have just implemented and why."`  

# Push work
You will need to `push` your changes back to the repository so they can be reviewed and accepted.

### Pull commits
> Check yourself! You need to make sure there are no conflix to resolve first.
> In these steps you will pull down any changes that where committed ahead of you.  

- Pull and resolve any conflicts. Contact us on [Discord](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/Discord) for help!  
  - Locate your Repository folder
  - Right click and select `Git Bash Here`
  - Run `git pull`

### Push your work for merge request.
> In these steps we explain again to commit any changes and then push back to the repository.

- Next you will need to commit your changes.
  - Locate your Repository folder
  - Right click and select `Git Bash Here`
  - Run `git commit -m "#73 Meaningful comment about the work I have just implemented and why."` 
  - Run `git push`
 
# FAQ
**Q: I am working on an Issue but I find I need to modify another class not in the issue, how do I approach this?**  
*A: Check the Class Board to see if its currently being worked on. If it is then contact the asignee.*  
   *If not depending on the weight of work either add a label for the class to your current card or create a new issue for assignment.*  
   *Use the filename of the Class preceeded with an underscore for sorting reasons. ie: `_BP_PlayerCharacter`*  


**Q: My Issue requires new assets, how do I go about this?**  
*A: New Assets will need to first travel down the [dizzydanglers-assets](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-assets) pipeline*
