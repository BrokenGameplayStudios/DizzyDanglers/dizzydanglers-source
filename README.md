# README.MD
How to help with this project...
## Intro
![enter image description here](http://dizzydanglers.com/wp-content/uploads/2020/05/dd-header.png)
Welcome to the [Dizzy Danglers Project](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source), if you are reading this then you are interested in getting involved somehow with this project. Take a moment to read the following information to better enhance your knowledge on what this project is about and how to help.

My name is __Brian Pulaski__, I've been involved with video games all my life, I have them to thank for my career in the IT Industry. Having dabbled in game development the past 5 years on a passion project called Ultraball. I've learned that game development has become far more approachable thanks to the tools in Unreal Engine 4. Now at the wee-young-age of 41 it's become my motivation to share what I've learned.  Having a live-in nephew has really put a spot light on importance of future opportunities. 

>__"It's become my motivation to share what I've learned."__

My experiences developing __Ultraball__ have lead me back full circle to my childhood. The game mechanics involved seem to cater to a sport I grew up playing, __Hockey__. 

The plan for this project is to remain open and in active development. We provide the software, and development tools for self-starter communities. We make the game, you help provide the servers and community that plays. Opportunity is shared across the community to learn how to get into __#gamedev__. 

That said as much development of this game will be left in the open public for users to observe and ask questions.
## What we're doing...
### Game.
A [Hockey]([https://en.wikipedia.org/wiki/Hockey](https://en.wikipedia.org/wiki/Hockey)) video game where you take on the role of an aspiring hockey youth in your local hockey club. Find arenas online to play against other player and teams, or hone your expertise while playing offline against bots.

Overall the game should represent a feeling of joining smaller communities when selecting a server. The tools in the server should support the ability for the owner to find creative ways to track stats, host leader boards,  tournaments, or leagues.

Compiled versions will be shipped free.
### Mission.
Not only are we attempting to make a fun video game for the public to enjoy for free; but this project acts as a community for its members to help educate and encourage others to get involved and learn [#gamedev]([https://en.wikipedia.org/wiki/Video_game_development](https://en.wikipedia.org/wiki/Video_game_development)).
## How we're going to do it...
We  start with the foundation, through good communication on our [Discord]([https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/Discord](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/Discord)), design and planning on our [wiki]([https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/home](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/wikis/home)), and tracking feature implementation on our [Gitlab]([https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source))
### Who
> "Talent wins games, but teamwork and intelligence win championships."  
> – Michael Jordan

Current we are looking for any individual(s) with a love for Hockey and/or game development. Whatever your experience level we are here to both educate where we can and take kindly to receiving knowledge. 
## Take Action!
### Now
> "Time you enjoy wasting is not wasted time."  
> ― Marthe Troly-Curtin

The time to take action is now! If you have even read this far then you likely possess at the least the curiosity, Join us on [Discord](https://discord.dizzydanglers.com) @ ___#development-discussion___. Tell us about yourself and how we can help you.
# Getting Started
> This is just a reminder of the steps.  
> Review the readme documentation for more detailed instructions.

## Install Unreal Engine
> Download and Install
- Unreal Engine Pre-compiled v4.25.0 @ [https://www.unrealengine.com/en-US/get-now](https://www.unrealengine.com/en-US/get-now)
- Be sure the correct version of engine on upper right corner.
## Install Git
> Download & Install 
- Git @ [https://git-scm.com/]
- Git-lfs @ [https://git-lfs.github.com/]
## Clone Repository
![enter image description here](http://dizzydanglers.com/wp-content/uploads/2020/05/Annotation_GitBashHere.png)  
~~~~
Example if destination is [c:\Git\DizzyDanglers]  
In File explorer inside of folder [c:\Git\]  
Right click and select [Git Bash Here]
~~~~
![enter image description here](http://dizzydanglers.com/wp-content/uploads/2020/05/Annotation_clonerepo.png)  
Run the following command: 
`git clone git@gitlab.com:BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source.git`  

## Find an Issue
> We use [Issues](https://docs.gitlab.com/ee/user/project/issues/) to track bugs and any change being done to the project.

- Read [CONTRIBUTING.md](CONTRIBUTING.md)
- Find an [issue](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/issues) on [board](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-source/-/boards) to see if there are any problems worth investigating.

## Open the project.
> Now that you got something to do, or if you just want to take a look.  
> Open the project file.  

- Use DizzyDanglers.uproject ![enter image description here](http://dizzydanglers.com/wp-content/uploads/2020/05/Annotation_ddprojicon.png)  
# Other
## Asset Source Files
- Asset Source Repository - [https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-assets](https://gitlab.com/BrokenGameplayStudios/DizzyDanglers/dizzydanglers-assets)

# More information on how to use Unreal?
- Unreal Engine 4 Documentation @ [https://docs.unrealengine.com/en-US/index.html](https://docs.unrealengine.com/en-US/index.html)
- Blueprint Communications | Live Training | Unreal Engine @ [https://www.youtube.com/watch?v=EM_HYqQdToE](https://www.youtube.com/watch?v=EM_HYqQdToE)
- Unreal Slackers Discord @ [https://discord.gg/unreal-slackers](https://discord.gg/unreal-slackers)
- Matt Wadstein Reference Videos [https://www.youtube.com/channel/UCOVfF7PfLbRdVEm0hONTrNQ](https://www.youtube.com/channel/UCOVfF7PfLbRdVEm0hONTrNQ)